﻿drop table if exists veiculo cascade;
drop table if exists modelo cascade;

create table veiculo (
nu_placa		char(7)		not null,
vl_ano_fabricacao	smallint 	not null,
dt_aquisicao		timestamp	not null,
cd_modelo		integer		not null,
cd_grupo		integer		not null,
constraint pk_veiculo
	primary key (nu_placa),
constraint ch_veiculo_ano_fabricacao
	check (vl_ano_fabricacao > 2000));

create table modelo (
cd			serial 		not null,    --int , create sequence, set default
nm			varchar(30)	not null,
constraint pk_modelo
	primary key (cd),
constraint un_modelo_nm
	unique (nm));

	
alter table veiculo
	add constraint fk_veiculo_modelo
		foreign key (cd_modelo)
		references modelo;
